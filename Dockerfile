# Stage 1: Build the application
FROM maven:3.8.4-openjdk-17 AS build
WORKDIR /app
COPY pom.xml .
COPY src ./src
RUN mvn clean install

# Stage 2: Run the application
FROM openjdk:17-jdk

WORKDIR /app

COPY --from=build /app/target/cicd-gitlub-application-1.0.jar /demo.jar

EXPOSE 8080

CMD ["java", "-jar", "demo.jar"]

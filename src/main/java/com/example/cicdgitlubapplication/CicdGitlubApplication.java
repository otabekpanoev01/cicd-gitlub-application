package com.example.cicdgitlubapplication;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CicdGitlubApplication {

    public static void main(String[] args) {
        SpringApplication.run(CicdGitlubApplication.class, args);
    }

}
